var Twitter = require('twitter');
var auth = require('./auth.js')


var app = require('express')();
var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
var http = require('http').Server(app);
var io = require('socket.io')(http);

var stats = {
    'tweets_number': {
        'macron': 0,
        'lepen': 0,
        'blanc': 0
    }
}
app.post('/listen', function(req, res){
  console.log(req.body.text);
  if (req.body.text != undefined){
    text = req.body.text;
      if (text.indexOf('Macron') != -1){
        stats.tweets_number.macron += 1;
      }
      if (text.indexOf('Le Pen') != -1){
        stats.tweets_number.lepen += 1;
      }
      if (text.indexOf('blanc') != -1){
        stats.tweets_number.blanc += 1;
      } 
    io.emit('stats', stats);
  }
  //io.emit('tweet', data); 
});

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
	console.log('a user connected');
});



http.listen(3000, function(){
  console.log('listening on *:3000');
});

