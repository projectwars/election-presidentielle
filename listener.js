var Twitter = require('twitter');
var auth = require('./auth.js');
var request = require('request');

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/listener_index.html');
});

io.on('connection', function(socket){
    console.log('a user connected');
});

http.listen(4000, function(){
  console.log('listening on *:4000');
});


var client = new Twitter(auth);
client.stream('statuses/filter', {track: 'avote,lepen,macron,jevote,mlp'}, function(stream) {
  stream.on('data', function(event) {
    console.log(event && event.text);
    request.post({url:'http://localhost:3000/listen', form:event},function callback(err, httpResponse, body) {
      if (err) {
        return console.error('consumer is offline', err);
      }
      console.log('Tweet sent to consumer server', body);
    });
    var data = { place: event.place,
                 coordinates: event.coordinates,
                 userLocation: event.user.location }
    io.emit('tweet', data);
  });

  stream.on('error', function(error) {
    throw error;
  });
});